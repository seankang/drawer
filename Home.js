import React, {Component} from 'react';
import {View, Text} from 'react-native';

import {createBottomTabNavigator, createAppContainer} from 'react-navigation';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import {Icon} from 'react-native-elements';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Profile from './Profile';
import Cart from './Cart';

class Home extends Component {
  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{fontSize: 40}}> Sean Home Screen</Text>
      </View>
    );
  }
}
const TabNavigator = createMaterialBottomTabNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      tabBarLabel: 'Home',
      activeColor: '#FF0000',
      inactiveColor: '#000000',
      barStyle: {backgroundColor: '#67baf6'},
      tabBarIcon: () => (
        <View>
          <Icon name={'home'} size={25} style={{color: '#FF0000'}} />
        </View>
      ),
    },
  },

  Profile: {
    screen: Profile,
    navigationOptions: {
      tabBarLabel: 'Profile',
      activeColor: '#FF0000',
      inactiveColor: '#000000',
      barStyle: {backgroundColor: '#67baf6'},
      tabBarIcon: () => (
        <View>
          <Icon name={'person'} size={25} style={{color: '#FF0000'}} />
        </View>
      ),
    },
  },
});

export default createAppContainer(TabNavigator);
